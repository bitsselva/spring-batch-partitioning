# What is this repository for?

This POC is to explore "Remote Parallel Processing" feature provided by Spring Batch. Also, to identify the best
deployment pattern in the modern Cloud Environment

# Architecture - Components

![Architecture][1]

[1]:  https://raw.githubusercontent.com/selva-dharmaraj/draw/main/parallel_spring_job_components.png

## Job Controller:

This component is responsible to initialize the job by deploying queue server, manager and worker application
dynamically. Also, this application monitor the job running status and delete application (pods) after successful
completion. This is a Java Spring boot application deployed in Kebernetes regions. This application can be scheduled to
start job in a particular time.

_Note: In the example app, an API - Endpoint is exposed to trigger job instead for scheduler._

## HTTP Queue Manager:

This is simple java based Queue Manager application works on HTTP protocol. This application acts as integration media
between Manager and Worker job processors. Manager job publish partition details through this application to the
available workers. All Workers periodically connects and process the work and report the completion status.

_Note: This components can be easily switched by any industry standard Queue Managers like Active MQ, RabbitMQ..._

## Manager:

Manager application is responsible for,

- Partition the data to be processed
- Communicate the partition details with Workers through Queue Manager
- Monitor the worker for partition completion
- Report the job completion summary
- Auto exit once after successful job completion

## Worker:

Worker application is responsible for,

- Process the partition received from Queue Manager
- Report the partition processing status
- Loop the process until no more partition available in the Queue Manager.
- Auto exit once after successful job completion

---

# Architecture - Remote Parallel Processing

![Architecture][2]

[2]:  https://raw.githubusercontent.com/selva-dharmaraj/draw/main/spring-batch-parallel-processing.png

## Job processing steps:

1. Manager read record counts to be processed from the Data Store
2. Manager apply partition logic (Custom) and identify partitions. Publish the partition details Queue Manager
3. Workers read partition data from Queue Manager and process it
4. Workers report the partition processing status back to Job Repository
5. Manager monitor workers process status. End the job after successful completion of all partitions

---

# Architecture - Cloud Deployment

![Architecture][3]

[3]:  https://raw.githubusercontent.com/selva-dharmaraj/draw/main/parallel_spring_job_deployments.png

## How to deploy and run this job in Kubernetes environment?

### Prerequisite:

- Kubernetes Cluster
- Docker
- Docker Image Repository

### Steps:

- Build and upload application in to image repository
- Create your application namespace
- Create Service Account in the namespace with appropriate access to deploy application
- Deploy 'Job Controller' application in the namespace
- Start (Trigger) the Job by calling '/deployQ' endpoint of the 'Job Controller' application

---

# How do I get set up?

## Prerequisite:

The single code base supports all components described in the above section. Each of the components should be started
using its own profile as described below.

### MySQL

- Please install MySQL in your local desktop.
- Create table "Account"

```
create table account
  (
  account_id bigint auto_increment
  primary key,
  token varchar(36) null,
  eligibility tinyint(1) null,
  create_date datetime null,
  update_date datetime null
  )
  comment 'Account Details';
```

- Load sample data (**_batchdb_account.csv_**)
- Update you access details in Application.yml in Resource folder.

## Commands:

### HTTP Queue Manager:

```
java \
    -jar \
    -Dspring.profiles.active=queueServer \
    -Dspring.main.web-application-type=servlet \
     target\spring-batch-partitioning-0.0.1-SNAPSHOT.jar
```

### Manager:

```
java \
    -jar \
    -Dspring.profiles.active=manager \
    target\spring-batch-partitioning-0.0.1-SNAPSHOT.jar
```

### Worker:

```
java 
    -jar \
    -Dspring.profiles.active=worker \
    target\spring-batch-partitioning-0.0.1-SNAPSHOT.jar
```

### Build your image:

```
mvn compile jib:build -Djib.to.image=cellva/partition-job:v2
```


