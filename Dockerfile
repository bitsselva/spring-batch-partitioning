FROM openjdk:8-jdk-alpine
ARG JAR_FILE=target/*.jar
COPY ${JAR_FILE} app.jar
# start command override with kubernetes pod argument
ENTRYPOINT ["java","-jar","/app.jar"]
