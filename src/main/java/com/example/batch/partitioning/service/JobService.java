package com.example.batch.partitioning.service;

import com.example.batch.partitioning.constant.Constant;
import com.example.batch.partitioning.model.JobCurrentStatus;
import com.example.batch.partitioning.model.JobStatusSummary;
import io.fabric8.kubernetes.api.model.PodList;
import io.fabric8.kubernetes.api.model.apps.Deployment;
import io.fabric8.kubernetes.api.model.batch.Job;
import io.fabric8.kubernetes.client.Config;
import io.fabric8.kubernetes.client.DefaultKubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClient;
import io.fabric8.kubernetes.client.KubernetesClientException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter.SseEventBuilder;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

@Service
@Slf4j
public class JobService {

  @Autowired RestTemplate httpQueueServerClient;

  @Value("${service.queue.url}")
  private String httpQueueServerUrl;

  private KubernetesClient defaultClient;

  private Deployment applyDeployment(Deployment deployment) {
    return getDefaultKubernetesClient()
        .apps()
        .deployments()
        .inNamespace(Constant.NAMESPACE)
        .createOrReplace(deployment);
  }

  private Job applyJob(Job job) {
    return getDefaultKubernetesClient()
        .batch()
        .jobs()
        .inNamespace(Constant.NAMESPACE)
        .createOrReplace(job);
  }

  private io.fabric8.kubernetes.api.model.Service applyService(
      io.fabric8.kubernetes.api.model.Service service) {
    return getDefaultKubernetesClient()
        .services()
        .inNamespace(Constant.NAMESPACE)
        .createOrReplace(service);
  }

  private KubernetesClient getDefaultKubernetesClient() {
    try (KubernetesClient client = new DefaultKubernetesClient()) {
      log.info("Kubernetes client created with default configuration");
      defaultClient = client;
      Config configuration = defaultClient.getConfiguration();
      log.info("------------------------------------");
      log.info("API Url: {}", configuration.getMasterUrl());
      log.info("Token: {}", configuration.getOauthToken());
      log.info("Namespace: {}", configuration.getNamespace());
      log.info("------------------------------------");
    } catch (KubernetesClientException ex) {
      ex.printStackTrace();
    }
    return defaultClient;
  }

  public void listPods() {
    try {

      getDefaultKubernetesClient()
          .pods()
          .inNamespace(Constant.NAMESPACE)
          .list()
          .getItems()
          .forEach(pod -> log.info(pod.getMetadata().getName()));

    } catch (Exception ex) {
      // Handle exception
      ex.printStackTrace();
    }
  }

  public void loadAndCreateService(SseEmitter emitter) throws IOException, InterruptedException {
    logAndEmitMessage(
        emitter, "Loading service for queue server from " + Constant.QUEUE_SERVER_SERVICE);
    io.fabric8.kubernetes.api.model.Service svc = loadService(Constant.QUEUE_SERVER_SERVICE);
    logAndEmitMessage(emitter, "Service loaded successfully!");
    logAndEmitMessage(
        emitter, "Applying Queue Server Service in the namespace: " + Constant.NAMESPACE);
    applyService(svc);
    logAndEmitMessage(emitter, "Service Created or Updated Successfully");
  }

  public void loadAndCreateWorkerJobs(SseEmitter emitter) throws IOException, InterruptedException {
    logAndEmitMessage(emitter, "Loading worker job from " + Constant.WORKER_JOB);
    Job workerJob = loadJob(Constant.WORKER_JOB);
    logAndEmitMessage(emitter, "Worker job loaded successfully!");
    logAndEmitMessage(emitter, "Applying worker job in the namespace: " + Constant.NAMESPACE);
    applyJob(workerJob);
    logAndEmitMessage(emitter, "Worker Job applied! Checking the running status");
    Thread.sleep(10000);
    PodList podListByLabel = getPodListByLabel("job-name=worker-job");
    waitForUntilPodInRunning(podListByLabel, emitter);
  }

  public void loadAndCreateManagerJob(SseEmitter emitter) throws IOException, InterruptedException {
    logAndEmitMessage(emitter, "Loading manager job from " + Constant.MANAGER_JOB);
    Job managerJob = loadJob(Constant.MANAGER_JOB);
    logAndEmitMessage(emitter, "Manager job loaded successfully!");
    logAndEmitMessage(emitter, "Applying manager job in the namespace: " + Constant.NAMESPACE);
    applyJob(managerJob);
    logAndEmitMessage(emitter, "Manager Job applied! Checking the running status");
    Thread.sleep(5000);
    PodList podListByLabel = getPodListByLabel("job-name=manager-job");
    waitForUntilPodInRunning(podListByLabel, emitter);
  }

  public void loadAndCreateDeployment(SseEmitter emitter) throws IOException, InterruptedException {
    logAndEmitMessage(
        emitter, "Creating Queue Server deployment from " + Constant.QUEUE_SERVER_DEPLOYMENT);
    Deployment deployment = loadDeployment(Constant.QUEUE_SERVER_DEPLOYMENT);
    logAndEmitMessage(emitter, "Queue server deployment loaded successfully");
    logAndEmitMessage(
        emitter, "Applying Queue server deployment in the namespace: " + Constant.NAMESPACE);
    applyDeployment(deployment);
    logAndEmitMessage(emitter, "Queue server deployed! Checking the running status");
    Thread.sleep(5000);
    PodList podList = getPodListByLabel("app=queueserver");
    waitForUntilPodInRunning(podList, emitter);
  }

  private void logAndEmitMessage(SseEmitter emitter, String messageToLog) throws IOException {

    SseEventBuilder event =
        SseEmitter.event().data(LocalDateTime.now().toString() + ": " + messageToLog);
    emitter.send(event);

    log.info(messageToLog);
  }

  public SseEmitter startJob() {

    SseEmitter emitter = new SseEmitter(600000L);

    ExecutorService sseMvcExecutor = Executors.newSingleThreadExecutor();
    sseMvcExecutor.execute(
        () -> {
          try {
            loadAndCreateService(emitter);
            loadAndCreateDeployment(emitter);
            loadAndCreateWorkerJobs(emitter);
            loadAndCreateManagerJob(emitter);
            monitorJobStatus(emitter);
            deleteResources(emitter);
            emitter.complete();
          } catch (Exception ex) {
            emitter.completeWithError(ex);
          }
        });
    return emitter;
  }

  private void deleteResources(SseEmitter emitter) throws IOException, InterruptedException {
    Thread.sleep(10000);
    logAndEmitMessage(
        emitter, "Deleting Jobs " + Constant.JOB_MANAGER + " and " + Constant.JOB_WORKER);
    getDefaultKubernetesClient()
        .batch()
        .jobs()
        .inNamespace(Constant.NAMESPACE)
        .withName(Constant.JOB_WORKER)
        .delete();
    getDefaultKubernetesClient()
        .batch()
        .jobs()
        .inNamespace(Constant.NAMESPACE)
        .withName(Constant.JOB_MANAGER)
        .delete();
    logAndEmitMessage(emitter, "Deleting Apps " + Constant.APP_QUEUE_SERVER);
    getDefaultKubernetesClient()
        .apps()
        .deployments()
        .inNamespace(Constant.NAMESPACE)
        .withName(Constant.APP_QUEUE_SERVER)
        .delete();
  }

  private void monitorJobStatus(SseEmitter emitter) throws InterruptedException, IOException {
    JobCurrentStatus status = JobCurrentStatus.IN_PROGRESS;
    while (!status.equals(JobCurrentStatus.COMPLETED)) {
      ResponseEntity<JobStatusSummary> response =
          httpQueueServerClient.getForEntity(
              httpQueueServerUrl + "/job/statusSummary", JobStatusSummary.class);
      HttpStatus statusCode = response.getStatusCode();
      JobStatusSummary summary = response.getBody();
      logAndEmitMessage(emitter, summary.toString());
      status = summary.getJobStatus();
      Thread.sleep(5000);
    }
  }

  private Deployment loadDeployment(String deploymentFile) {

    return getDefaultKubernetesClient()
        .apps()
        .deployments()
        .load(JobService.class.getResourceAsStream(deploymentFile))
        .get();
  }

  private io.fabric8.kubernetes.api.model.Service loadService(String serviceFile) {

    return getDefaultKubernetesClient()
        .services()
        .load(JobService.class.getResourceAsStream(serviceFile))
        .get();
  }

  private Job loadJob(String jobFile) {

    return getDefaultKubernetesClient()
        .batch()
        .jobs()
        .load(JobService.class.getResourceAsStream(jobFile))
        .get();
  }

  private void waitForUntilPodInRunning(PodList podList, SseEmitter emitter) throws IOException {
    podList.getItems().stream()
        .parallel()
        .forEach(
            pod -> {
              try {
                String podName = pod.getMetadata().getName();
                logAndEmitMessage(
                    emitter,
                    "Checking pod {"
                        + podName
                        + "} status. Initial Status is "
                        + pod.getStatus().getPhase());
                getDefaultKubernetesClient()
                    .pods()
                    .inNamespace(Constant.NAMESPACE)
                    .withName(podList.getItems().get(0).getMetadata().getName())
                    .waitUntilCondition(
                        p -> p.getStatus().getPhase().equals("Running"), 1, TimeUnit.MINUTES);
                String tailLog =
                    getDefaultKubernetesClient()
                        .pods()
                        .inNamespace(Constant.NAMESPACE)
                        .withName(podName)
                        .tailingLines(10)
                        .getLog();
                logAndEmitMessage(
                    emitter, "Last 10 line of pod {" + podName + "}:\n {" + tailLog + "}");
              } catch (InterruptedException | IOException e) {
                e.printStackTrace();
              }
            });
  }

  private PodList getPodListByLabel(String label) {
    return getDefaultKubernetesClient()
        .pods()
        .inNamespace(Constant.NAMESPACE)
        .withLabel(label)
        .list();
  }
}
