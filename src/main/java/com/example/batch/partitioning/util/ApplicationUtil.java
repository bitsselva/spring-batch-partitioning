package com.example.batch.partitioning.util;

import java.util.Date;

public class ApplicationUtil {

  public static String findDifference(Date startDate, Date endDate) {
    StringBuffer sb = new StringBuffer();
    try {
      long difference_In_Time = endDate.getTime() - startDate.getTime();

      long difference_In_Seconds = (difference_In_Time / 1000) % 60;
      long difference_In_Minutes = (difference_In_Time / (1000 * 60)) % 60;
      long difference_In_Hours = (difference_In_Time / (1000 * 60 * 60)) % 24;
      long difference_In_Years = (difference_In_Time / (1000l * 60 * 60 * 24 * 365));
      long difference_In_Days = (difference_In_Time / (1000 * 60 * 60 * 24)) % 365;

      /*
            sb.append(difference_In_Years);
            sb.append(" years, ");
      */
      sb.append(difference_In_Days);
      sb.append(" day(s), ");
      sb.append(difference_In_Hours);
      sb.append(" hour(s), ");
      sb.append(difference_In_Minutes);
      sb.append(" minute(s), ");
      sb.append(difference_In_Seconds);
      sb.append(" second(s)");
    } catch (Exception e) {
      e.printStackTrace();
    }
    return sb.toString();
  }
}
