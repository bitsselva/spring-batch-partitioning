package com.example.batch.partitioning.constant;

import java.util.UUID;

public class Constant {

  // Worker
  public static final String WORKER_NAME = "Worker-" + UUID.randomUUID().toString();
  // Job Controller
  public static final String NAMESPACE = "default";
  public static final String QUEUE_SERVER_DEPLOYMENT = "/kube/queueserver-deployment.yml";
  public static final String QUEUE_SERVER_SERVICE = "/kube/queueserver-service.yml";
  public static final String WORKER_JOB = "/kube/partitionjob-worker.yml";
  public static final String MANAGER_JOB = "/kube/partitionjob-manager.yml";
  public static final String APP_QUEUE_SERVER = "queueserver-app";
  public static final String APP_JOB_CONTROLLER_SERVER = "jobcontroller-app";
  public static final String SERVICE_QUEUE_SERVER = "queueserver-service";
  public static final String JOB_WORKER = "worker-job";
  public static final String JOB_MANAGER = "manager-job";
}
