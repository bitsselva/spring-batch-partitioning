package com.example.batch.partitioning;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.JobParameters;
import org.springframework.batch.core.JobParametersBuilder;
import org.springframework.batch.core.JobParametersInvalidException;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.repository.JobExecutionAlreadyRunningException;
import org.springframework.batch.core.repository.JobInstanceAlreadyCompleteException;
import org.springframework.batch.core.repository.JobRestartException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

@Component
@Profile("manager")
@Slf4j
public class BatchCommandLineRunner implements CommandLineRunner {

  @Autowired
  JobLauncher jobLauncher;
  @Autowired
  Job accountProcessingJob;
  @Autowired
  ApplicationContext applicationContext;

  @Override
  public void run(String... args) throws Exception {

    System.out.println("Starting Batch Job with Unique Parameter");

    JobParameters param = new JobParametersBuilder().addString("JobID",
        String.valueOf(System.currentTimeMillis())).toJobParameters();
    try {
      jobLauncher.run(accountProcessingJob, param);
      log.info("Job completed. Manager is shutting down.........");
      shutdownChannel(0);
    } catch (JobExecutionAlreadyRunningException e) {
      e.printStackTrace();
    } catch (JobRestartException e) {
      e.printStackTrace();
    } catch (JobInstanceAlreadyCompleteException e) {
      e.printStackTrace();
    } catch (JobParametersInvalidException e) {
      e.printStackTrace();
    }

  }

  private void shutdownChannel(int exitCode) throws InterruptedException {
    Thread.sleep(2000);
    System.exit(exitCode);
  }

}
