package com.example.batch.partitioning.mapper;

import com.example.batch.partitioning.model.Account;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import org.springframework.jdbc.core.RowMapper;

public class AccountMapper implements RowMapper<Account> {


  @Override
  public Account mapRow(ResultSet resultSet, int i) throws SQLException {

    return Account.builder()
        .accountId(resultSet.getLong("account_id"))
        .token(resultSet.getString("token"))
        .eligibility(resultSet.getBoolean("eligibility"))
        .createDate(new Timestamp(resultSet.getDate("create_date").getTime()).toLocalDateTime())
        .updateDate(new Timestamp(resultSet.getDate("update_date").getTime()).toLocalDateTime())
        .build();
  }
}
