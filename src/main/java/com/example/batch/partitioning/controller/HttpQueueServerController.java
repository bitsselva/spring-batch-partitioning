package com.example.batch.partitioning.controller;

import com.example.batch.partitioning.model.JobCurrentStatus;
import com.example.batch.partitioning.model.JobInfo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.integration.partition.StepExecutionRequest;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Queue;

@Profile({"queueServer"})
@RestController
@Slf4j
@RequestMapping("/partition")
public class HttpQueueServerController {
  private JobInfo jobInfo = new JobInfo();

  @PostMapping(
      path = "/manager/work",
      produces = MediaType.APPLICATION_JSON_VALUE,
      consumes = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  public ResponseEntity<Boolean> addPartition(@RequestBody StepExecutionRequest partition) {

    if (jobInfo.getJobId() != null
        && partition.getJobExecutionId().longValue() != jobInfo.getJobId().longValue()) {
      log.info("New job to process. Reset the previous job information");
      jobInfo.resetQueue();
    }

    log.info("Adding Partition: {}", partition);
    final boolean add = jobInfo.addPartitionToQueue(partition);
    log.info("Partition added successfully!");
    return new ResponseEntity(add, HttpStatus.CREATED);
  }

  @PostMapping(
      path = "/worker/work/completed",
      produces = MediaType.APPLICATION_JSON_VALUE,
      consumes = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  public ResponseEntity<Boolean> partitionCompleted(
      @RequestHeader("worker-name") String workerName, @RequestBody Long stepId) {
    log.info("Step completed: {}", stepId);
    boolean updateStatus = jobInfo.updatePartitionCompletedStatus(workerName, stepId);
    if (jobInfo.isAllPartitionCompleted()) {
      log.info("Job Finished!");
      jobInfo.setJobCurrentStatus(JobCurrentStatus.COMPLETED);
    }
    log.info("Step completion updated successfully!");
    return new ResponseEntity(updateStatus, HttpStatus.OK);
  }

  @RequestMapping(path = "/worker/work", produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  public ResponseEntity<StepExecutionRequest> getPartition(
      @RequestHeader("worker-name") String workerName) {

    // Job not started
    if (jobInfo.isJobNotStarted()) {
      log.info("Request for work. But, job not started");
      jobInfo.getWorkers().add(workerName);
      return new ResponseEntity(null, HttpStatus.NO_CONTENT);
    }
    // Job completed. Queue is empty
    else if (jobInfo.isNoMoreWork()) {
      log.info("Request for work. But, no work available");
      jobInfo.getWorkers().add(workerName);
      return new ResponseEntity(
          jobInfo.getPartitions().entrySet().stream().findFirst().get().getValue(),
          HttpStatus.ACCEPTED);
    }
    log.info("Request for work. Returning work");
    // Job to do. Return work for the request
    jobInfo.setJobCurrentStatus(JobCurrentStatus.IN_PROGRESS);
    StepExecutionRequest remove = jobInfo.getPartitionToProcess(workerName);
    log.info("Returning partition: {}", remove);
    return new ResponseEntity(remove, HttpStatus.OK);
  }

  @RequestMapping(path = "/job/status", produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  public ResponseEntity<JobInfo> getJobStatus() {
    return new ResponseEntity(jobInfo, HttpStatus.OK);
  }

  @RequestMapping(path = "/job/statusSummary", produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  public ResponseEntity<String> getJobStatusSummary() {
    return new ResponseEntity(jobInfo.jobStatusSummary(), HttpStatus.OK);
  }

  @RequestMapping(path = "/partitions", produces = MediaType.APPLICATION_JSON_VALUE)
  @ResponseBody
  public ResponseEntity<Queue<StepExecutionRequest>> listPartitions() {
    log.info("Request for available partition...");
    return new ResponseEntity(jobInfo.getPartitionProcessingQueue(), HttpStatus.OK);
  }
}
