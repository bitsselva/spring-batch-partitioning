package com.example.batch.partitioning.controller;

import com.example.batch.partitioning.service.JobService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.SseEmitter;

@Profile({"jobController"})
@RestController
@Slf4j
public class JobController {

  @Autowired private JobService jobService;

  @GetMapping(path = "/deployQ")
  public SseEmitter deployQueueServer() {
    log.info("Request to list jobs");
    return jobService.startJob();
  }
}
