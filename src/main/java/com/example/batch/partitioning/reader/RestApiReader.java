package com.example.batch.partitioning.reader;

import com.example.batch.partitioning.model.Account;
import java.time.Duration;
import java.util.Arrays;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.item.ItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@StepScope
public class RestApiReader implements ItemReader<Account> {

  private static final Logger LOGGER = LoggerFactory.getLogger(RestApiReader.class);
  //  private final String apiUrl = "http://localhost:8081/employees";
  private int nextStudentIndex;
  private List<Account> studentData;
  @Value("#{jobParameters['employeeRestApi']}")
  private String employeeRestApi;
  @Autowired
  private RestTemplate restTemplate;

  @Override
  public Account read() throws Exception {
    LOGGER.info("Reading the information of the next student");
    if (studentDataIsNotInitialized()) {
      studentData = fetchAccountDataFromAPI();
    }
    Account nextStudent = null;
    if (nextStudentIndex < studentData.size()) {
      nextStudent = studentData.get(nextStudentIndex);
      nextStudentIndex++;
    }
    LOGGER.info("Found student: {}", nextStudent);
    return nextStudent;
  }

  private boolean studentDataIsNotInitialized() {
    return this.studentData == null;
  }

  @Bean
  @StepScope
  public RestTemplate restTemplate(RestTemplateBuilder builder) {
    return builder
        .setConnectTimeout(Duration.ofMillis(3000))
        .setReadTimeout(Duration.ofMillis(3000))
        .build();
  }

  private List<Account> fetchAccountDataFromAPI() {
    LOGGER.info("Fetching student data from an external API by using the url: {}", employeeRestApi);
    ResponseEntity<Account[]> response =
        restTemplate.getForEntity(employeeRestApi, Account[].class);
    Account[] studentData = response.getBody();
    LOGGER.info("Got {} students", studentData.length);
    return Arrays.asList(studentData);
  }
}
