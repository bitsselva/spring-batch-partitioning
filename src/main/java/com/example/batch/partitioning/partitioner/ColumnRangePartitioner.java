package com.example.batch.partitioning.partitioner;

import com.example.batch.partitioning.repository.AccountRepository;
import java.util.HashMap;
import java.util.Map;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class ColumnRangePartitioner implements Partitioner {

  @Autowired private AccountRepository accountRepository;

  @Override
  public Map<String, ExecutionContext> partition(int gridSize) {
    log.info("Partitioning accounts based on the range...");

    int min = accountRepository.min().intValue();
    int max = accountRepository.max().intValue();
    int targetSize = (max - min) / gridSize + 1;
    log.info("-----------------------------------------------------------------------------------");
    log.info("Accounts Range: {} - {}", min, max);
    log.info("Planned workers: {}", gridSize);
    log.info("Records for each worker {}", targetSize);

    Map<String, ExecutionContext> result = new HashMap<>();

    int number = 0;
    int start = min;
    int end = start + targetSize - 1;

    while (start <= max) {
      ExecutionContext value = new ExecutionContext();
      result.put("partition" + number, value);

      if (end >= max) {
        end = max;
      }
      log.info("Partition " + number + " Account Fetch Range: " + start + " - " + end);
      value.putInt("minValue", start);
      value.putInt("maxValue", end);

      start += targetSize;
      end += targetSize;

      number++;
    }
    log.info("-----------------------------------------------------------------------------------");
    // loadAccounts();
    return result;
  }

  /*private void loadAccounts() {
    for (int i = 0; i < 100000; i++) {

      accountRepository
          .save(Account.builder().token(UUID.randomUUID().toString()).eligibility(false).createDate(
              LocalDateTime.now()).updateDate(LocalDateTime.now()).build());

    }
  }*/
}
