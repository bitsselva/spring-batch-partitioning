package com.example.batch.partitioning.partitioner.handler;

import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.explore.support.JobExplorerFactoryBean;
import org.springframework.batch.core.partition.PartitionHandler;
import org.springframework.batch.core.partition.StepExecutionSplitter;
import org.springframework.batch.integration.partition.StepExecutionRequest;
import org.springframework.batch.poller.DirectPoller;
import org.springframework.batch.poller.Poller;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.integration.channel.QueueChannel;
import org.springframework.messaging.PollableChannel;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.web.client.RestTemplate;

import javax.sql.DataSource;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.Set;
import java.util.concurrent.Callable;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

@Slf4j
@Profile("manager")
public class HttpMessageChannelPartitionHandler implements PartitionHandler, InitializingBean {

  @Value("${service.queue.url}")
  private String queueUrl;

  private int gridSize = 1;
  private String stepName;
  private long pollInterval = 10000L;
  private JobExplorer jobExplorer;
  private boolean pollRepositoryForResults = false;
  private long timeout = -1L;
  private DataSource dataSource;
  private PollableChannel replyChannel;
  @Autowired private RestTemplate httpQueueServerClient;

  public HttpMessageChannelPartitionHandler() {}

  private void sendMessage(StepExecutionRequest requestMessage) {

    boolean result =
        (boolean)
            httpQueueServerClient.postForObject(
                queueUrl + "/manager/work", requestMessage, Boolean.class);
    log.info("Published message: {}", result);
  }

  public void afterPropertiesSet() throws Exception {
    Assert.notNull(this.stepName, "A step name must be provided for the remote workers.");
    this.pollRepositoryForResults = this.dataSource != null || this.jobExplorer != null;
    if (this.pollRepositoryForResults) {
      log.info(
          "MessageChannelPartitionHandler is configured to poll the job repository for worker results");
    }

    if (this.dataSource != null && this.jobExplorer == null) {
      JobExplorerFactoryBean jobExplorerFactoryBean = new JobExplorerFactoryBean();
      jobExplorerFactoryBean.setDataSource(this.dataSource);
      jobExplorerFactoryBean.afterPropertiesSet();
      this.jobExplorer = jobExplorerFactoryBean.getObject();
    }

    if (!this.pollRepositoryForResults && this.replyChannel == null) {
      this.replyChannel = new QueueChannel();
    }
  }

  public void setJobExplorer(JobExplorer jobExplorer) {
    this.jobExplorer = jobExplorer;
  }

  public void setPollInterval(long pollInterval) {
    this.pollInterval = pollInterval;
  }

  public void setDataSource(DataSource dataSource) {
    this.dataSource = dataSource;
  }

  public void setGridSize(int gridSize) {
    this.gridSize = gridSize;
  }

  public void setStepName(String stepName) {
    this.stepName = stepName;
  }

  public Collection<StepExecution> handle(
      StepExecutionSplitter stepExecutionSplitter, StepExecution masterStepExecution)
      throws Exception {
    Set<StepExecution> split = stepExecutionSplitter.split(masterStepExecution, this.gridSize);
    if (CollectionUtils.isEmpty(split)) {
      return split;
    } else {
      int count = 0;

      StepExecutionRequest request;
      for (Iterator var5 = split.iterator(); var5.hasNext(); sendMessage(request)) {
        StepExecution stepExecution = (StepExecution) var5.next();

        request =
            new StepExecutionRequest(
                this.stepName, stepExecution.getJobExecutionId(), stepExecution.getId());

        if (log.isDebugEnabled()) {
          log.debug("Sending request: " + request);
        }
      }

      return this.pollReplies(masterStepExecution, split);
    }
  }

  private Collection<StepExecution> pollReplies(
      final StepExecution masterStepExecution, final Set<StepExecution> split) throws Exception {
    final Collection<StepExecution> result = new ArrayList(split.size());
    Callable<Collection<StepExecution>> callback =
        new Callable<Collection<StepExecution>>() {
          public Collection<StepExecution> call() throws Exception {
            Iterator stepExecutionIterator = split.iterator();

            while (stepExecutionIterator.hasNext()) {
              StepExecution curStepExecution = (StepExecution) stepExecutionIterator.next();
              if (!result.contains(curStepExecution)) {
                StepExecution partitionStepExecution =
                    HttpMessageChannelPartitionHandler.this.jobExplorer.getStepExecution(
                        masterStepExecution.getJobExecutionId(), curStepExecution.getId());
                if (!partitionStepExecution.getStatus().isRunning()) {
                  result.add(partitionStepExecution);
                }
              }
            }

            log.info("Currently waiting on {} partitions to finish", split.size());

            if (result.size() == split.size()) {
              return result;
            } else {
              return null;
            }
          }
        };
    Poller<Collection<StepExecution>> poller = new DirectPoller(this.pollInterval);
    Future<Collection<StepExecution>> resultsFuture = poller.poll(callback);
    return this.timeout >= 0L
        ? (Collection) resultsFuture.get(this.timeout, TimeUnit.MILLISECONDS)
        : (Collection) resultsFuture.get();
  }
}
