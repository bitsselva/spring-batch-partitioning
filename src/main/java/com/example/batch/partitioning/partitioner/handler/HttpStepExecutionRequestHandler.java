package com.example.batch.partitioning.partitioner.handler;

import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobInterruptedException;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.step.NoSuchStepException;
import org.springframework.batch.core.step.StepLocator;
import org.springframework.batch.integration.partition.StepExecutionRequest;

public class HttpStepExecutionRequestHandler {

  private JobExplorer jobExplorer;
  private StepLocator stepLocator;

  public HttpStepExecutionRequestHandler() {}

  public void setStepLocator(StepLocator stepLocator) {
    this.stepLocator = stepLocator;
  }

  public void setJobExplorer(JobExplorer jobExplorer) {
    this.jobExplorer = jobExplorer;
  }

  public StepExecution handle(StepExecutionRequest request) {
    Long jobExecutionId = request.getJobExecutionId();
    Long stepExecutionId = request.getStepExecutionId();
    StepExecution stepExecution =
        this.jobExplorer.getStepExecution(jobExecutionId, stepExecutionId);
    if (stepExecution == null) {
      throw new NoSuchStepException(
          "No StepExecution could be located for this request: " + request);
    } else {
      String stepName = request.getStepName();
      Step step = this.stepLocator.getStep(stepName);
      if (step == null) {
        throw new NoSuchStepException(
            String.format("No Step with name [%s] could be located.", stepName));
      } else {
        try {
          step.execute(stepExecution);
        } catch (JobInterruptedException var8) {
          stepExecution.setStatus(BatchStatus.STOPPED);
        } catch (Throwable var9) {
          stepExecution.addFailureException(var9);
          stepExecution.setStatus(BatchStatus.FAILED);
        }

        return stepExecution;
      }
    }
  }
}
