package com.example.batch.partitioning.partitioner;

import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.partition.support.Partitioner;
import org.springframework.batch.item.ExecutionContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.ResourcePatternResolver;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class CustomResourcePartitioner implements Partitioner {

  @Autowired ResourcePatternResolver resourcePatternResolver;

  @Override
  public Map<String, ExecutionContext> partition(int gridSize) {
    log.info("Inside partitioner. Grid Size is: " + gridSize);
    Map<String, ExecutionContext> map = new HashMap<>(gridSize);
    AtomicInteger partitionNumber = new AtomicInteger(1);
    Resource[] resources;

    try {
      resources = resourcePatternResolver.getResources("classpath*:/*.txt");
      Arrays.stream(resources)
          .forEach(
              file -> {
                ExecutionContext context = new ExecutionContext();
                System.out.println("filename: " + file.getFilename());
                if (file.getFilename().startsWith("transaction-data")) {
                  context.putString("fileName", file.getFilename());
                  context.putString("outputFile", "output_" + file.getFilename());
                  map.put("partition" + partitionNumber.getAndIncrement(), context);
                }
              });
      log.info("Partitions Created: " + map.size());
    } catch (IOException e) {
      e.printStackTrace();
    }
    return map;
  }
}
