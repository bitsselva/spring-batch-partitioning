package com.example.batch.partitioning.repository;

import com.example.batch.partitioning.model.Account;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface AccountRepository extends JpaRepository<Account, Long> {

  @Query(value = "SELECT min(accountId) FROM Account ")
  Long min();

  @Query(value = "SELECT max(accountId) FROM Account ")
  Long max();

}
