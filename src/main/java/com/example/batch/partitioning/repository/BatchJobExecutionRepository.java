package com.example.batch.partitioning.repository;

import com.example.batch.partitioning.model.BatchJobExecution;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface BatchJobExecutionRepository extends JpaRepository<BatchJobExecution, Long> {

  BatchJobExecution findBatchJobExecutionByJobExecutionId(Long jobExecutionId);
}
