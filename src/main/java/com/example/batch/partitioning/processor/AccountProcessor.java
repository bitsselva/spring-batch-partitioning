package com.example.batch.partitioning.processor;

import com.example.batch.partitioning.model.Account;
import java.time.LocalDateTime;
import java.util.Random;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.item.ItemProcessor;

@Slf4j
public class AccountProcessor implements ItemProcessor<Account, Account> {

  @Override
  public Account process(Account item) throws Exception {
    if (new Random().nextBoolean()) {
      item.setEligibility(true);
      item.setUpdateDate(LocalDateTime.now());
      //      log.info("Eligible [YES] Account {}", item.toString());
      return item;
    }
    //    log.info("Eligible [NO]  Account {}", item.toString());
    return null;
  }
}
