package com.example.batch.partitioning;

import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableBatchProcessing(modular = true)
// @IntegrationComponentScan
// @EnableScheduling
public class BatchPartitioningApplication {

  public static void main(String[] args) {
    SpringApplication.run(BatchPartitioningApplication.class, args);
  }
}
