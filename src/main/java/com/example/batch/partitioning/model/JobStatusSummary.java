package com.example.batch.partitioning.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
@Slf4j
public class JobStatusSummary {
  private Long jobId;
  private JobCurrentStatus jobStatus;
  private Integer partitions;
  private Integer workers;
  private Integer inProgressPartitions;
  private Integer completedPartitions;
}
