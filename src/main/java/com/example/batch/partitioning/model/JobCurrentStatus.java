package com.example.batch.partitioning.model;

public enum JobCurrentStatus {
  NOT_STARTED,
  IN_PROGRESS,
  COMPLETED;
}
