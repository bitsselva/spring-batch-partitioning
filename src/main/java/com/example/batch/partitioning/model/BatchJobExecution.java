package com.example.batch.partitioning.model;

import java.time.LocalDateTime;
import javax.persistence.Entity;
import javax.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
@Entity
public class BatchJobExecution {

  @Id
  private Long jobExecutionId;
  private Long version;
  private Long jobInstanceId;
  private LocalDateTime createTime;
  private LocalDateTime startTime;
  private LocalDateTime endTime;
  private LocalDateTime lastUpdate;
  private String status;
  private String exitMessage;

}
