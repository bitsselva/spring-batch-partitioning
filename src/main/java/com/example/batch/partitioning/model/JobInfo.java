package com.example.batch.partitioning.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.integration.partition.StepExecutionRequest;

import java.util.*;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicInteger;

@Data
@AllArgsConstructor
@Builder
@NoArgsConstructor
@Slf4j
public class JobInfo {
  private Long jobId;
  private AtomicInteger partitionCount = new AtomicInteger(0);
  private Map<Long, StepExecutionRequest> partitions = new HashMap<>();
  private JobCurrentStatus jobCurrentStatus = JobCurrentStatus.NOT_STARTED;
  private Map<String, ArrayList<StepExecutionRequest>> completedPartitions = new HashMap<>();
  private Map<String, ArrayList<StepExecutionRequest>> inProgressPartitions = new HashMap<>();
  private Set<String> workers = new HashSet<>();
  private BlockingQueue<StepExecutionRequest> partitionProcessingQueue =
      new LinkedBlockingQueue<>(1024);

  public boolean addPartitionToQueue(StepExecutionRequest stepExecutionRequest) {
    try {
      partitionProcessingQueue.put(stepExecutionRequest);
      partitions.put(stepExecutionRequest.getStepExecutionId(), stepExecutionRequest);
      partitionCount.incrementAndGet();
      setJobId(stepExecutionRequest.getJobExecutionId());
    } catch (InterruptedException e) {
      e.printStackTrace();
      return false;
    }
    return true;
  }

  public StepExecutionRequest getPartitionToProcess(String workerName) {
    try {
      workers.add(workerName);
      StepExecutionRequest stepExecutionRequest = partitionProcessingQueue.take();
      updateJobProgressInfo(workerName, stepExecutionRequest, inProgressPartitions);
      return stepExecutionRequest;
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    return null;
  }

  public boolean isAllPartitionCompleted() {
    return getPartitionProcessingQueue().isEmpty()
        && getPartitionStatusCount(inProgressPartitions) == 0;
  }

  public boolean isNoMoreWork() {
    return getPartitionProcessingQueue().isEmpty()
        && (getJobCurrentStatus().equals(JobCurrentStatus.IN_PROGRESS)
            || getJobCurrentStatus().equals(JobCurrentStatus.COMPLETED));
  }

  public boolean isJobNotStarted() {
    return getPartitionProcessingQueue().isEmpty()
        && getJobCurrentStatus().equals(JobCurrentStatus.NOT_STARTED);
  }

  public JobStatusSummary jobStatusSummary() {

    return JobStatusSummary.builder()
        .jobId(getJobId())
        .jobStatus(getJobCurrentStatus())
        .partitions(getPartitions().size())
        .workers(getWorkers().size())
        .inProgressPartitions(getPartitionStatusCount(inProgressPartitions))
        .completedPartitions(getPartitionStatusCount(completedPartitions))
        .build();
  }

  private int getPartitionStatusCount(
      Map<String, ArrayList<StepExecutionRequest>> partitionContainer) {
    return partitionContainer.entrySet().stream().mapToInt(x -> x.getValue().size()).sum();
  }

  public boolean updatePartitionCompletedStatus(String workerName, Long stepId) {
    boolean remove = false;
    try {
      StepExecutionRequest stepExecutionRequest = partitions.get(stepId);
      remove = inProgressPartitions.get(workerName).remove(stepExecutionRequest);
      updateJobProgressInfo(workerName, stepExecutionRequest, completedPartitions);

    } catch (Exception e) {
      e.printStackTrace();
    }
    return remove;
  }

  private void updateJobProgressInfo(
      String key,
      StepExecutionRequest partitionRequest,
      Map<String, ArrayList<StepExecutionRequest>> partitionContainer) {
    if (partitionContainer.containsKey(key)) {
      ArrayList<StepExecutionRequest> stepExecutionRequests = partitionContainer.get(key);
      stepExecutionRequests.add(partitionRequest);
    } else {
      ArrayList<StepExecutionRequest> list = new ArrayList<>();
      list.add(partitionRequest);
      partitionContainer.put(key, list);
    }
  }

  public void resetQueue() {
    jobId = null;
    partitionCount.set(0);
    partitions.clear();
    jobCurrentStatus = JobCurrentStatus.NOT_STARTED;
    completedPartitions.clear();
    inProgressPartitions.clear();
    workers.clear();
    partitionProcessingQueue.clear();
  }
}
