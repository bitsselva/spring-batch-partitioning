package com.example.batch.partitioning.configuration;

import com.example.batch.partitioning.constant.Constant;
import com.example.batch.partitioning.listener.CustomStepExecutionListener;
import com.example.batch.partitioning.mapper.AccountMapper;
import com.example.batch.partitioning.model.Account;
import com.example.batch.partitioning.partitioner.handler.HttpStepExecutionRequestHandler;
import com.example.batch.partitioning.processor.AccountProcessor;
import com.example.batch.partitioning.repository.AccountRepository;
import com.example.batch.partitioning.repository.BatchJobExecutionRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepScope;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.launch.JobLauncher;
import org.springframework.batch.core.launch.support.SimpleJobLauncher;
import org.springframework.batch.core.repository.JobRepository;
import org.springframework.batch.integration.partition.BeanFactoryStepLocator;
import org.springframework.batch.integration.partition.StepExecutionRequest;
import org.springframework.batch.integration.partition.StepExecutionRequestHandler;
import org.springframework.batch.item.data.RepositoryItemWriter;
import org.springframework.batch.item.data.builder.RepositoryItemWriterBuilder;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.database.JdbcPagingItemReader;
import org.springframework.batch.item.database.Order;
import org.springframework.batch.item.database.builder.JdbcBatchItemWriterBuilder;
import org.springframework.batch.item.database.support.MySqlPagingQueryProvider;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.RequestEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.client.RestTemplate;

import javax.sql.DataSource;
import java.net.URI;
import java.util.HashMap;
import java.util.Map;

@Slf4j
@Profile("worker")
@EnableScheduling
@Configuration
public class WorkerStepConfiguration {

  @Autowired public StepBuilderFactory workerStepBuilderFactory;

  @Autowired public JobExplorer jobExplorer;

  @Autowired ApplicationContext applicationContext;

  @Autowired private JdbcPagingItemReader<Account> accountReader;

  @Autowired private RepositoryItemWriter<Account> accountWriter;

  @Autowired private JdbcBatchItemWriter<Account> accountBatchWriter;

  @Autowired private AccountProcessor accountProcessor;

  @Autowired private AccountRepository accountRepository;

  @Autowired private DataSource datasource;

  @Autowired private RestTemplate httpQueueServerClient;

  @Autowired private BatchJobExecutionRepository batchJobExecutionRepository;

  @Value("${service.queue.url}")
  private String queueUrl;

  @Bean
  @StepScope
  @Qualifier("accountReader")
  public JdbcPagingItemReader<Account> accountReader(
      @Value("#{stepExecutionContext['minValue']}") Long minValue,
      @Value("#{stepExecutionContext['maxValue']}") Long maxValue) {
    log.info("Reading from {} to {}", minValue, maxValue);

    Map<String, Order> sortKeys = new HashMap<>();
    sortKeys.put("account_id", Order.ASCENDING);

    MySqlPagingQueryProvider queryProvider = new MySqlPagingQueryProvider();
    queryProvider.setSelectClause("account_id, token, eligibility, create_date,update_date");
    queryProvider.setFromClause("from account");
    queryProvider.setWhereClause(
        "where account_id >= " + minValue + " and account_id <= " + maxValue);
    queryProvider.setSortKeys(sortKeys);

    JdbcPagingItemReader<Account> reader = new JdbcPagingItemReader<>();
    reader.setDataSource(this.datasource);
    reader.setPageSize(50);
    reader.setRowMapper(new AccountMapper());
    reader.setQueryProvider(queryProvider);

    return reader;
  }

  @Bean
  @StepScope
  public AccountProcessor processor() {
    return new AccountProcessor();
  }

  @Bean
  @StepScope
  @Qualifier("accountWriter")
  public RepositoryItemWriter<Account> accountWriter() {
    return new RepositoryItemWriterBuilder<Account>()
        .repository(accountRepository)
        .methodName("save")
        .build();
  }

  @Bean
  @StepScope
  @Qualifier("accountBatchWriter")
  public JdbcBatchItemWriter<Account> accountBatchWriter() {
    return new JdbcBatchItemWriterBuilder<Account>()
        .beanMapped()
        //        .itemSqlParameterSourceProvider(new
        // BeanPropertyItemSqlParameterSourceProvider<>())
        .dataSource(this.datasource)
        .sql(
            "UPDATE account SET eligibility = :eligibility, update_date = :updateDate WHERE account_id = :accountId")
        .build();
  }

  @Bean(name = "launcher")
  public JobLauncher jobLauncher(JobRepository jobRepo) {
    SimpleJobLauncher simpleJobLauncher = new SimpleJobLauncher();
    simpleJobLauncher.setJobRepository(jobRepo);
    return simpleJobLauncher;
  }

  @Bean
  public Step workerStep(CustomStepExecutionListener listener) {
    return workerStepBuilderFactory
        .get("workerStep")
        .listener(listener)
        .<Account, Account>chunk(50)
        .reader(accountReader)
        .processor(accountProcessor)
        .writer(accountBatchWriter)
        .build();
  }

  public StepExecutionRequestHandler stepExecutionRequestHandler() {
    StepExecutionRequestHandler stepExecutionRequestHandler = new StepExecutionRequestHandler();
    BeanFactoryStepLocator stepLocator = new BeanFactoryStepLocator();
    stepLocator.setBeanFactory(this.applicationContext);
    stepExecutionRequestHandler.setStepLocator(stepLocator);
    stepExecutionRequestHandler.setJobExplorer(this.jobExplorer);

    return stepExecutionRequestHandler;
  }

  //  @Profile("worker")
  //  @Bean
  @Scheduled(fixedDelay = 2000, initialDelay = 10000)
  public void scheduleTaskLookup() {
    log.info("Looking up for a job to do from Manager...");
    StepExecutionRequest workerRequestFromQueue = getWorkerRequestFromQueue();
    if (workerRequestFromQueue != null) {
      log.info("Start processing job...");
      processWorkerJob(workerRequestFromQueue);
    }
  }

  private void processWorkerJob(StepExecutionRequest workerRequestFromQueue) {
    HttpStepExecutionRequestHandler stepExecutionRequestHandler =
        new HttpStepExecutionRequestHandler();
    BeanFactoryStepLocator stepLocator = new BeanFactoryStepLocator();
    stepLocator.setBeanFactory(this.applicationContext);
    stepExecutionRequestHandler.setStepLocator(stepLocator);
    stepExecutionRequestHandler.setJobExplorer(this.jobExplorer);
    stepExecutionRequestHandler.handle(workerRequestFromQueue);
  }

  private StepExecutionRequest getWorkerRequestFromQueue() {

    RequestEntity<Void> request =
        RequestEntity.post(URI.create(queueUrl + "/worker/work"))
            .accept(MediaType.APPLICATION_JSON)
            // any other headers
            .header("worker-name", Constant.WORKER_NAME)
            .build();

    ResponseEntity<StepExecutionRequest> response =
        httpQueueServerClient.exchange(request, StepExecutionRequest.class);
    /*
    ResponseEntity<StepExecutionRequest> response =
        partitionClient.getForEntity(queueUrl, StepExecutionRequest.class);
    */
    HttpStatus statusCode = response.getStatusCode();
    StepExecutionRequest work = response.getBody();
    log.info("Response Code: {} and step to process {}", statusCode, work);
    // HttpStatus.OK - Got a work to do
    if (statusCode.equals(HttpStatus.OK)) {
      log.info(
          "Got a work {} to do from my manager for a job {}",
          work.getStepExecutionId(),
          work.getJobExecutionId());
      return response.getBody();
    }
    // HttpStatus.ACCEPTED - No work yet available
    else if (statusCode.equals(HttpStatus.NO_CONTENT)) {
      log.info("No work received from manager");
      return response.getBody();
    }
    // HttpStatus.ACCEPTED - Previously processed job was completed
    else if (statusCode.equals(HttpStatus.ACCEPTED)) {
      Long jobId = response.getBody().getJobExecutionId();
      String jobStatus = batchJobExecutionRepository.findById(jobId).get().getStatus();
      if ("COMPLETED".equals(jobStatus)) {
        log.info(
            "JOB ({}) Finished successfully with the status of '{}'! Shutting down the worker",
            jobId,
            jobStatus);
        System.exit(0);
      } else {
        log.info(
            "Processed job [{}] status is => {}. Waiting for the job to complete",
            jobId,
            jobStatus);
      }
    }
    return null;
  }
}
