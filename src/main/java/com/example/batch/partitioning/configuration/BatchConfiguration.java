package com.example.batch.partitioning.configuration;

import com.example.batch.partitioning.listener.CustomJobExecutionListener;
import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Profile;

@Profile("manager")
@Configuration
@Import(value = {ManagerStepConfiguration.class, WorkerStepConfiguration.class})
public class BatchConfiguration {

  @Autowired
  public JobBuilderFactory jobBuilderFactory;

  @Autowired
  @Qualifier("managerStep")
  private Step managerStep;

  @Bean
  @Profile("manager")
  public Job accountProcessingJob(CustomJobExecutionListener listener) throws Exception {
    return jobBuilderFactory.get("ExamplePartitionJob")
        .listener(listener)
        .flow(managerStep)
        .end()
        .build();
  }
}
