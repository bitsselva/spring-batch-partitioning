package com.example.batch.partitioning.configuration;

import com.example.batch.partitioning.partitioner.ColumnRangePartitioner;
import com.example.batch.partitioning.partitioner.handler.HttpMessageChannelPartitionHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.explore.JobExplorer;
import org.springframework.batch.core.partition.PartitionHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

@Profile("manager")
@Slf4j
public class ManagerStepConfiguration {

  @Autowired public JobExplorer jobExplorer;
  @Autowired public StepBuilderFactory managerStepBuilderFactory;

  @Autowired private ColumnRangePartitioner columnRangePartitioner;

  @Autowired private PartitionHandler partitionHandler;

  @Bean
  public PartitionHandler partitionHandler() throws Exception {
    log.info("Create Partition Handler to co-ordinate manager and worker communication");
    HttpMessageChannelPartitionHandler partitionHandler = new HttpMessageChannelPartitionHandler();
    partitionHandler.setStepName("workerStep");
    partitionHandler.setGridSize(10);
    partitionHandler.setPollInterval(2000l);
    partitionHandler.setJobExplorer(this.jobExplorer);
    partitionHandler.afterPropertiesSet();

    return partitionHandler;
  }

  @Bean
  @Qualifier("managerStep")
  public Step managerStep() throws Exception {
    return managerStepBuilderFactory
        .get("managerStep")
        .partitioner("workerStep", columnRangePartitioner)
        .partitionHandler(partitionHandler)
        .build();
  }
}
