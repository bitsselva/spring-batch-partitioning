package com.example.batch.partitioning.listener;

import com.example.batch.partitioning.constant.Constant;
import com.example.batch.partitioning.util.ApplicationUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.ExitStatus;
import org.springframework.batch.core.StepExecution;
import org.springframework.batch.core.listener.StepExecutionListenerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
@Slf4j
public class CustomStepExecutionListener extends StepExecutionListenerSupport {
  @Autowired private RestTemplate httpQueueServerClient;

  @Value("${service.queue.url}")
  private String queueUrl;

  @Override
  public ExitStatus afterStep(StepExecution stepExecution) {

    log.error("----------------------------------------------------------------------------------");
    log.error(
        "StepExecutionListener - afterStep: processed records between {} and {}",
        stepExecution.getExecutionContext().getInt("minValue"),
        stepExecution.getExecutionContext().getInt("maxValue"));
    log.error("StepExecutionListener - afterStep:getStepName= {}", stepExecution.getStepName());
    log.error("StepExecutionListener - afterStep:getExitStatus= {}", stepExecution.getExitStatus());
    log.error("StepExecutionListener - afterStep:getReadCount= {}", stepExecution.getReadCount());
    //    log.error("StepExecutionListener - afterStep:getCommitCount=
    // {}",stepExecution.getCommitCount());
    log.error(
        "StepExecutionListener - afterStep:getFilterCount= {}", stepExecution.getFilterCount());
    log.error("StepExecutionListener - afterStep:getWriteCount= {}", stepExecution.getWriteCount());
    log.error(
        "StepExecutionListener - afterStep:getReadSkipCount= {}", stepExecution.getReadSkipCount());
    log.error(
        "StepExecutionListener - afterStep:getProcessSkipCount= {}",
        stepExecution.getProcessSkipCount());
    log.error(
        "StepExecutionListener - afterStep:getWriteSkipCount= {}",
        stepExecution.getWriteSkipCount());
    log.error(
        "StepExecutionListener - afterStep:getRollbackCount= {}", stepExecution.getRollbackCount());
    log.error("StepExecutionListener - afterStep:getSummary= {}", stepExecution.getSummary());
    log.error(
        "StepExecutionListener - afterStep:getFailureExceptions= {}",
        stepExecution.getFailureExceptions());
    log.error("StepExecutionListener - afterStep:getStartTime= {}", stepExecution.getStartTime());
    log.error(
        "StepExecutionListener - afterStep:getLastUpdated= {}", stepExecution.getLastUpdated());
    log.error("StepExecutionListener - afterStep:getEndTime= {}", stepExecution.getEndTime());
    log.error(
        "StepExecutionListener - afterStep:getRunTime= {}",
        ApplicationUtil.findDifference(
            stepExecution.getStartTime(), stepExecution.getLastUpdated()));
    log.error("----------------------------------------------------------------------------------");
    sendCompletedStatus(stepExecution.getId());
    return super.afterStep(stepExecution);
  }

  private void sendCompletedStatus(Long stepId) {

    HttpHeaders headers = new HttpHeaders();
    headers.set("worker-name", Constant.WORKER_NAME);

    HttpEntity<Long> request = new HttpEntity<>(stepId, headers);

    boolean result =
        (boolean)
            httpQueueServerClient.postForObject(
                queueUrl + "/worker/work/completed", request, Boolean.class);
    log.info("Sent completed status: {}", result);
  }

  @Override
  public void beforeStep(StepExecution stepExecution) {
    log.error("----------------------------------------------------------------------------------");
    log.error(
        "StepExecutionListener - beforeStep: request to process records between {} and {}",
        stepExecution.getExecutionContext().getInt("minValue"),
        stepExecution.getExecutionContext().getInt("maxValue"));
    log.error("----------------------------------------------------------------------------------");

    super.beforeStep(stepExecution);
  }
}
