package com.example.batch.partitioning.listener;

import com.example.batch.partitioning.util.ApplicationUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class CustomJobExecutionListener extends JobExecutionListenerSupport {

  @Autowired private TaskExecutor taskExecutor;

  @Override
  public void afterJob(JobExecution jobExecution) {
    if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
      log.info("---------------------------------------------------------------------------------");
      log.info("!!! JOB FINISHED! Time to verify the results");
      log.info(
          "JobExecutionListener - afterJob:getReadCount = {}",
          jobExecution.getStepExecutions().stream().mapToInt(exe -> exe.getReadCount()).sum());
      log.info(
          "JobExecutionListener - afterJob:getFilterCount = {}",
          jobExecution.getStepExecutions().stream().mapToInt(exe -> exe.getFilterCount()).sum());
      log.info(
          "JobExecutionListener - afterJob:getWriteCount = {}",
          jobExecution.getStepExecutions().stream().mapToInt(exe -> exe.getWriteCount()).sum());
      log.info(
          "JobExecutionListener - afterJob:getReadSkipCount = {}",
          jobExecution.getStepExecutions().stream().mapToInt(exe -> exe.getReadSkipCount()).sum());
      log.info(
          "JobExecutionListener - afterJob:getProcessSkipCount = {}",
          jobExecution.getStepExecutions().stream()
              .mapToInt(exe -> exe.getProcessSkipCount())
              .sum());
      log.info(
          "JobExecutionListener - afterJob:getWriteSkipCount = {}",
          jobExecution.getStepExecutions().stream().mapToInt(exe -> exe.getWriteSkipCount()).sum());
      log.info("StepExecutionListener - afterJob:getStartTime = {}", jobExecution.getStartTime());
      log.info("StepExecutionListener - afterJob:getEndTime = {}", jobExecution.getEndTime());
      log.info(
          "StepExecutionListener - afterJob:getRunTime = {}",
          ApplicationUtil.findDifference(jobExecution.getStartTime(), jobExecution.getEndTime()));
      log.info("---------------------------------------------------------------------------------");
    } else if (jobExecution.getStatus() == BatchStatus.FAILED) {
      log.error("!!! JOB FAILED, please check details for more information");
    }
  }

  @Override
  public void beforeJob(JobExecution jobExecution) {
    log.error("----------------------------------------------------------------------------------");
    log.error("JobExecutionListener - beforeStep: request to process records");
    log.error("----------------------------------------------------------------------------------");

    super.beforeJob(jobExecution);
  }
}
